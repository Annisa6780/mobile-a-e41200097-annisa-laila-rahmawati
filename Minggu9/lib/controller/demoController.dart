import 'dart:html';

import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';
import '/model/product.dart';
import 'package:get/get.dart';

class DemoController extends GetxController {
  var cartItem = <Product>[].obs;
  int get cartCount => cartItem.length;
  double get totalAmount => cartItem.fold(0.0, 
    (sum, element) => sum + element.price);
  addToCart(Product product) {
    cartItem.add(product);
  }

  final storage = GetStorage();
  bool get isDark => storage.read('dartmode') ?? false;
  ThemeData get theme => isDark ? ThemeData.dark() : ThemeData.light();
  void changeTheme(bool val) => storage.write('dartmode', val);
}