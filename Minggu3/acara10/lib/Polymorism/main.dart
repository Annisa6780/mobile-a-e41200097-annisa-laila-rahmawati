
import 'bangunDatar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
 BangunDatar lingkaran = new Lingkaran(23);
 print(lingkaran.keliling());
 print(lingkaran.luas());

 BangunDatar persegi = new Persegi(5);
 print(persegi.keliling());
 print(persegi.luas());

 BangunDatar segitiga = new Segitiga(4,3,5);
  print(segitiga.keliling());
  print(segitiga.luas());
}
