import 'amorTitan.dart';
import 'attackTitan.dart';
import 'beastTitan.dart';
import 'human.dart';
import 'titan.dart';

void main() {
  AmorTitan amorTitan = new AmorTitan();
  AttackTitan attackTitan = new AttackTitan();
  BeastTitan beastTitan = new BeastTitan();
  Human human = new Human();

  amorTitan.powerPoint = 4;
  attackTitan.powerPoint = 8;
  beastTitan.powerPoint = 6;
  human.powerPoint = 2;

  printPowerPoint("Amor Titan ", amorTitan.powerPoint);
  printPowerPoint("Attack Titan ", attackTitan.powerPoint);
  printPowerPoint("Beast Titan ", beastTitan.powerPoint);
  printPowerPoint("Human ", human.powerPoint);

  printSkill("Amor Titan ", amorTitan.terjang());
  printSkill("Attack Titan ", attackTitan.punch());
  printSkill("Beast Titan ", beastTitan.lempar());
  printSkill("Human ", human.killAllTitan());
}

printPowerPoint(type, powerPoint) {
  print("Power point dari $type adalah $powerPoint");
}

printSkill(type, skill) {
  print("$type mengeluarkan special skill : $skill");
}